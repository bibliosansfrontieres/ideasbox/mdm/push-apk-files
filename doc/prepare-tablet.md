# Préparation de la tablette

Ce qui suit est à faire pour chaque tablette.

## Premier démarrage

La tablette vient de se prendre un reset d'usine. Elle présente un écran « `Bienvenue !` »

On suit les instructions :

* Accepter les CGU - mais ne pas « `Accepter tout` », il ne faut pas cocher la checkbox « `Recevoir des informations commerciales` »
* Connecter la tablette au réseau wifi du coin

La tablette finit par prendre sa configuration : on voit un fond d'écran ideascube.

## Configurer un compte Google

`Paramètres` -> `Cloud et comptes` -> `Ajouter un compte` -> `Google`

Utiliser les identifiants fournis.

* Un identifiant pour 5 tablettes

## Activer la connexion par USB

Il faut activer le mode "Développeur"

`Paramètres` -> `À propos` -> `Informations sur le logiciel`

Appuyer 7 fois sur le `Numéro de version`. Au bout de quelques appuis, on a un message « `Plus que 3 fois avant d'activer le mode développeur` », puis enfin, « `Le mode développeur a été activé` ».

Suite à cette opération, dans les menus des Paramètres, l'entrée `Options pour les développeurs` apparait.

On va donc dans ce nouveau menu, et on active le `Débugage USB`.

## Brancher la tablette au laptop

Quand on branche la tablette, sur celle-ci apparait un message « `Autoriser le débogage USB ?` » qu'on accepte, bien entendu.

La tablette est désormais prête à être manipulée via le script.
